<?php

class Receta
{
  public $id;
  public $nombre;
  public $tipo;
  public $elaboracion;
  public $tiempo;
  public $imagen;

  public function __construct($id, $data, $img)
  {
    $this->id = $id;
    $this->nombre = trim($data["nombre"]);
    $this->tipo = $data["tipo"];
    $this->elaboracion = trim($data["ela"]);
    $this->tiempo = trim($data["tiempo"]);
    $this->imagen = $img;
  }

  public static function getId($db, $nombre)
  {
    try
    {
      $query = "SELECT * FROM ricorico_receta WHERE nombre = :nombre COLLATE NOCASE";
      $stmt = $db->prepare($query);
      $stmt->execute(array(':nombre' => $nombre));
      $resultado = $stmt->fetch();
    }
    catch(PDOException $e)
    {
      echo $e->getMessage();
    }

    return $resultado["id"];
  }

  public static function eliminarIng($db, $id)
  {
    try
    {
      $delete = "DELETE FROM ricorico_receta_ingredientes WHERE receta_id = :id";
      $stmt = $db->prepare($delete);
      $stmt->execute(array("id" => $id));
    }
    catch(PDOException $e)
    {
      echo $e->getMessage();
    }
  }

  public function guardar($db)
  {
    try
    {
      $datos = array('userid_id' => $this->id,
                    'nombre' => $this->nombre,
                    'tipo' => $this->tipo,
                    'elaboracion' => $this->elaboracion,
                    'tiempo' => $this->tiempo,
                    'fecha_creacion' => date("Y-m-d H:i:s"),
                    'fecha_edicion' => date("Y-m-d H:i:s"),
                    'imagen' => $this->imagen);

      $insert = "INSERT INTO ricorico_receta (userid_id, nombre, tipo, elaboracion, tiempo, fecha_creacion, fecha_edicion, imagen) 
                VALUES (:userid_id, :nombre, :tipo, :elaboracion, :tiempo, :fecha_creacion, :fecha_edicion, :imagen)";
      $stmt = $db->prepare($insert);
      $stmt->execute($datos);
    }
    catch (PDOException $e)
    {
      echo $e->getMessage();
    }
  }

  public static function borrar($db, $idUsuario, $idReceta)
  {
    // Obtenemos la receta por ID
    try
    {
      $query = "SELECT * FROM ricorico_receta WHERE id = :id";
      $stmt = $db->prepare($query);
      $stmt->execute(array(':id' => $idReceta));
      $resultado = $stmt->fetch();
    }
    catch(PDOException $e)
    {
      echo $e->getMessage();
    }

    // Si no es el admin ni el usuario que creó la receta se redirecciona
    if ($idUsuario != "1" && $resultado["userid_id"] != $idUsuario)
    {
      return false;
    }

    // Borramos la imagen de la receta
    unlink("static/" . $resultado["imagen"]);

    // Borramos la receta de la tabla ricorico_receta
    try
    {
      $delete = "DELETE FROM ricorico_receta WHERE id = :id";
      $stmt = $db->prepare($delete);
      $stmt->execute(array("id" => $idReceta));
    }
    catch (PDOException $e)
    {
      return false;
    }

    // Borramos las relaciones con los ingredientes
    // de la tabla ricorico_receta_ingredientes
    try
    {
      $delete = "DELETE FROM ricorico_receta_ingredientes WHERE receta_id = :id";
      $stmt = $db->prepare($delete);
      $stmt->execute(array("id" => $idReceta));
    }
    catch (PDOException $e)
    {
      echo $e->getMessage();
    }

    return true;
  }

  public function editar($db)
  {
    // Si se ha actualizado la imagen
    if ($this->imagen == '')
    {
      $datos = array('nombre' => $this->nombre,
                    'tipo' => $this->tipo,
                    'elaboracion' => $this->elaboracion,
                    'tiempo' => $this->tiempo,
                    'fecha_edicion' => date("Y-m-d H:i:s"),
                    'id' => $this->id);

      try
      {
        $update = "UPDATE ricorico_receta SET nombre=:nombre, tipo=:tipo, elaboracion=:elaboracion, tiempo=:tiempo, fecha_edicion=:fecha_edicion WHERE id=:id";
        $stmt = $db->prepare($update);
        $stmt->execute($datos);
      }
      catch (PDOException $e)
      {
        echo $e->getMessage();
      }
    }
    else
    {
      $datos = array('nombre' => $this->nombre,
                    'tipo' => $this->tipo,
                    'elaboracion' => $this->elaboracion,
                    'tiempo' => $this->tiempo,
                    'fecha_edicion' => date("Y-m-d H:i:s"),
                    'imagen' => $this->imagen,
                    'id' => $this->id);

      try
      {
        $update = "UPDATE ricorico_receta SET nombre=:nombre, tipo=:tipo, elaboracion=:elaboracion, tiempo=:tiempo, fecha_edicion=:fecha_edicion, imagen=:imagen WHERE id=:id";
        $stmt = $db->prepare($update);
        $stmt->execute($datos);
      }
      catch (PDOException $e)
      {
        echo $e->getMessage();
      }
    }
  }
}

?>