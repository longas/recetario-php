<?php

include 'pwdhash.php';

class Usuario
{
  public $username;
  public $password;

  public function __construct($username, $password)
  {
    $this->username = $username;
    $this->password = $password;
  }

  // Método para comprobar si el nombre esta libre
  private function usuarioYaRegis($db)
  {
    try
    {
      $query = "SELECT * FROM auth_user WHERE username = :user COLLATE NOCASE";
      $stmt = $db->prepare($query);
      $stmt->execute(array(':user' => $this->username));
      $resultado = $stmt->fetch();
    }
    catch(PDOException $e)
    {
      echo $e->getMessage();
    }

    return $resultado[0] > 0 ? true : false;
  }

  public function guardar($db)
  {
    if ($this->usuarioYaRegis($db))
    {
      return false;
    }

    // Encriptamos la contraseña
    $password = create_hash($this->password);
    $password = strtr($password, array(':' => '$'));
    $password = "pbkdf2_" . $password;

    try
    {
      $datos = array('password' => $password,
                    'last_login' => date("Y-m-d H:i:s"),
                    'is_superuser' => 0,
                    'username' => $this->username,
                    'first_name' => "",
                    'last_name' => "",
                    'email' => "",
                    'is_staff' => 1,
                    'is_active' => 1,
                    'date_joined' => date("Y-m-d H:i:s"));

      $insert = "INSERT INTO auth_user (password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined)
                VALUES (:password, :last_login, :is_superuser, :username, :first_name, :last_name, :email, :is_staff, :is_active, :date_joined)";
      $stmt = $db->prepare($insert);
      $stmt->execute($datos);
    }
    catch (PDOException $e)
    {
      echo $e->getMessage();
    }

    return true;
  }

  public function login($db)
  {
    // Obtenemos el usuario de la bbdd
    try
    {
      $query = "SELECT * FROM auth_user WHERE username = :user COLLATE NOCASE";
      $stmt = $db->prepare($query);
      $stmt->execute(array(':user' => $this->username));
      $resultado = $stmt->fetch();
    }
    catch (PDOException $e)
    {
      echo $e->getMessage();
    }

    // Comprobamos que haya un usuario con ese nombre
    if ($resultado[0] == 0)
    {
      return false;
    }

    // Comprobamos que la contraseña es la correcta
    $password = substr($resultado['password'], 7);

    if (validate_password($this->password, $password))
    {
      session_start();
      $_SESSION["id"] = $resultado['id'];
      $_SESSION["user"] = $resultado['username'];

      // Actualizamos la fecha del último login
      try
      {
        $update = "UPDATE auth_user SET last_login=:last_login WHERE id=:id";
        $stmt = $db->prepare($update);
        $stmt->execute(array("last_login" => date("Y-m-d H:i:s"), "id" => $resultado['id']));
      }
      catch(PDOException $e)
      {
        echo $e->getMessage();
      }

      return true;
    }
    else
    {
      return false;
    }
  }
}

?>