<?php

include 'twig.php';
include 'db.php';
include 'Ingrediente.php';
include 'Receta.php';

// Si el usuario no esta logeado redirigimos
if (!isset($_SESSION["id"])) {
  header('location: index');
  exit;
}

// Si se llama al archivo por POST
if ($_SERVER["REQUEST_METHOD"] == "POST")
{
  // Procesamos el archivo subido
  // Obtenemos el tipo de fichero
  $imgtipo = explode("/", $_FILES['img']['type'])[1];
  // Carpeta temporal del fichero
  $imgtmp = $_FILES['img']['tmp_name'];

  // Declaramos las extensiones admitidas
  $extensiones = array("jpeg", "jpg", "png");
  // Comprobamos que es una de ellas
  if (!in_array($imgtipo, $extensiones))
  {
    $error = "Tipo de archivo no válido, por favor elige un archivo JPEG o PNG.";
    echo $twig->render('add.html', array("error" => $error));
    return;
  }

  // Generamos un nombre aleatorio
  $imgnombre = sha1(time());

  // Creamos la carpeta si no existe
  if(!is_dir("static/images/platos")) mkdir("static/images/platos");

  // Movemos la imagen a nuestra carpeta
  $destinoimg = "images/platos/" . $imgnombre . "." . $imgtipo;
  move_uploaded_file($imgtmp, "static/" . $destinoimg);

  // Guardamos la receta
  $receta = new Receta($_SESSION['id'], $_POST, $destinoimg);
  $receta->guardar($db);

  // Procesamos los ingredientes
  // Eliminamos espacios al principio y final del string
  $ingredientes = trim($_POST['ing']);
  // Creamos un array separando las palabras por las comas
  $ingredientes = explode(",", $ingredientes);

  // Los guardamos en la bbdd
  foreach ($ingredientes as $ingrediente)
  {
    $ingrediente = trim($ingrediente);

    // Comprobamos que no sean solo espacios
    if (!ctype_space($ingrediente))
    {
      $ing = new Ingrediente($ingrediente);
      $ing->guardar($db);

      // Añadimos a la tabla ricorico_receta_ingredientes las relaciones
      // entre cada receta y sus ingredientes
      try
      {
        $insert = "INSERT INTO ricorico_receta_ingredientes (receta_id, ingrediente_id) VALUES (:receta_id, :ingrediente_id)";
        $stmt = $db->prepare($insert);
        $stmt->execute(array("receta_id" => Receta::getId($db, trim($_POST["nombre"])), "ingrediente_id" => Ingrediente::getId($db, $ingrediente)));
      }
      catch (PDOException $e)
      {
        echo $e->getMessage();
        return;
      }
    }
  }

  // Redirigimos a la receta
  header('location: detail/' . Receta::getId($db, trim($_POST["nombre"])));
  exit;
}

// Renderizamos el template
echo $twig->render('add.html');

?>