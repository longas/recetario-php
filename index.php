<?php

include 'db.php';
include 'twig.php';

// Obtenemos las recetas
try
{
  $query = "SELECT * FROM ricorico_receta ORDER BY fecha_creacion DESC";
  $stmt = $db->prepare($query);
  $stmt->execute();
  $result = $stmt->fetchAll();
}
catch (PDOException $e)
{
  echo $e->getMessage();
}

// Renderizamos el template
echo $twig->render('list.html', array('recetas' => $result));

?>