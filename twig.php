<?php

require_once 'vendor/autoload.php';

$loader = new Twig_Loader_Filesystem('templates');
$twig = new Twig_Environment($loader);

// Añadimos la variable SESSION globalmente a todos los templates
session_start();
$twig->addGlobal("session", $_SESSION);

?>