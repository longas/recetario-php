<?php

include 'db.php';
include 'twig.php';

// Si no se ha especificado el id devolvemos al index
if (empty($_GET['id']))
{
  header('location: index');
  exit;
}

// Obtenemos la ID
$id = $_GET['id'];

// Obtenemos la receta
try
{
  $query = "SELECT * FROM ricorico_receta WHERE id = :id";
  $stmt = $db->prepare($query);
  $stmt->execute(array(':id' => $id));
  $receta = $stmt->fetch();
}
catch(PDOException $e)
{
  echo $e->getMessage();
}

// Si no hay ninguna receta con esa id devolvemos al index
if ($receta[0] == 0)
{
  header('location: ../');
  exit;
}

// Obtenemos los ingredientes
try
{
  $query = "SELECT * FROM ricorico_ingrediente AS I JOIN ricorico_receta_ingredientes AS RI ON RI.receta_id = :id AND RI.ingrediente_id = I.id";
  $stmt = $db->prepare($query);
  $stmt->execute(array(':id' => $id));
  $ingredientes = $stmt->fetchAll();
}
catch(PDOException $e)
{
  echo $e->getMessage();
}

// Cerramos la conexión
$db = null;

// Renderizamos el template
echo $twig->render('detail.html', array('receta' => $receta, 'ingredientes' => $ingredientes));

?>