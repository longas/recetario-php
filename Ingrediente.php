<?php

class Ingrediente
{
  public $id;
  public $nombre;

  public function __construct($nombre)
  {
    $this->nombre = ucfirst($nombre);
  }

  public static function getId($db, $nombre)
  {
    try
    {
      $query = "SELECT * FROM ricorico_ingrediente WHERE nombre = :nombre COLLATE NOCASE";
      $stmt = $db->prepare($query);
      $stmt->execute(array(':nombre' => $nombre));
      $resultado = $stmt->fetch();
    }
    catch(PDOException $e)
    {
      echo $e->getMessage();
    }

    return $resultado["id"];
  }

  // Método para comprobar si el ingrediente ya existe en la bbdd
  private function ingredienteYaRegis($db)
  {
    try
    {
      $query = "SELECT * FROM ricorico_ingrediente WHERE nombre = :nombre COLLATE NOCASE";
      $stmt = $db->prepare($query);
      $stmt->execute(array(':nombre' => $this->nombre));
      $resultado = $stmt->fetch();
    }
    catch(PDOException $e)
    {
      echo $e->getMessage();
    }

    return $resultado[0] > 0 ? true : false;
  }

  public function guardar($db)
  {
    if ($this->ingredienteYaRegis($db))
    {
      return false;
    }

    try
    {
      $insert = "INSERT INTO ricorico_ingrediente (nombre) VALUES (:nombre)";
      $stmt = $db->prepare($insert);
      $stmt->execute(array("nombre" => $this->nombre));
    }
    catch (PDOException $e)
    {
      echo $e->getMessage();
    }

    return true;
  }
}

?>