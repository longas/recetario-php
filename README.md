Recetario con PHP
====================
Gabriel Longás
--------------

### Instalación y configuración
* Colocamos el proyecto en un servidor web.
* [Instalamos Composer](https://getcomposer.org/download/) en nuestra máquina.
* Instalamos las dependencias del proyecto (**$php composer.phar install**).
* En el archivo **templates/base.html**, cambiamos la dirección del tag **base** a donde hayamos colocado el proyecto.
* Nos aseguramos que la carpeta y los archivos tienen los permisos correctos de escritura (**$chmod -R 777 recetario-php**).
* Entramos en la dirección de nuestro servidor, [localhost](http://localhost) si estas trabajando en local.