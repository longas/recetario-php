<?php

include 'db.php';
include 'Receta.php';

// Si el usuario no esta logeado redirigimos
session_start();

if (!isset($_SESSION["id"]))
{
  header('location: ../');
  exit;
}

// Si no se ha especificado el id devolvemos al index
if (empty($_GET['id']))
{
  header('location: ../');
  exit;
}

// Obtenemos la ID
$idReceta = $_GET['id'];

// Borramos la receta
if (!Receta::borrar($db, $_SESSION["id"], $idReceta))
{
  header('location: ../');
  exit;
}

// Redirigimos al index
header('location: ../');

?>