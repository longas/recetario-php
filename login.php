<?php

include 'db.php';
include 'twig.php';
include 'Usuario.php';

// Si el usuario ya esta logeado redirigimos
if (isset($_SESSION["id"])) {
  header('location: index');
  exit;
}

// Si se llama al archivo por POST
if ($_SERVER["REQUEST_METHOD"] == "POST")
{
  // Capturamos los datos del form
  $user = trim($_POST['user']);
  $pass = trim($_POST['pass']);

  // Logeamos el usuario
  $usuario = new Usuario($user, $pass);

  if (!$usuario->login($db))
  {
    $error = "El usuario o la contraseña introducidos son incorrectos.";
    echo $twig->render('login.html', array("error" => $error));
    return;
  }

  // Redirigimos al index
  header('location: index');
  exit;
}

// Renderizamos el template
echo $twig->render('login.html');

?>