<?php

include 'db.php';
include 'twig.php';

// Si se llama al archivo por POST
if ($_SERVER["REQUEST_METHOD"] == "POST")
{
  // Creo las variables necesarias
  $datos = array();
  $query = "";
  $querystart = "SELECT R.* FROM ricorico_receta AS R";
  $inner = "";
  $primero = true;

  // Creamos el SELECT FROM WHERE
  foreach ($_POST as $key => $value)
  {
    // Si se buscar por nombre
    if ($key == "nombre" && !empty($value))
    {
      // Añadimos WHERE ya que nombre siempre será el primero
      $query .= " WHERE";
      // Añadimos el valor a buscar y el parámetro
      $query .= " R." . $key . " LIKE :" . $key . "";
      // Agregamos el elemento al array
      $datos[$key] = "%" . $value . "%";

      // Marcamos que ya no es el primero
      $primero = false;
    }

    // Si se ha puesto algo
    if ($value != "all")
    {
      // Si se busca por tipo
      if ($key == "tipo")
      {
        // Si es el primero añadimos WHERE y si no AND
        $query .= $primero ? " WHERE" : " AND";
        $query .= " R.tipo = :" . $key;
        $datos[$key] = $value;
        $primero = false;
      }

      // Si se busca por ingrediente
      if ($key == "ing")
      {
        // Añadimos los INNER JOINs para enlazar las tablas
        $inner .= " INNER JOIN ricorico_receta_ingredientes AS RI ON RI.receta_id = R.id 
                  INNER JOIN ricorico_ingrediente AS I ON I.id = RI.ingrediente_id";
        $query .= $primero ? " WHERE" : " AND";
        $query .= " I.nombre = :" . $key;
        $datos[$key] = $value;
        $primero = false;
      }

      // Si se busca por autor
      if ($key == "autor")
      {
        $inner .= " INNER JOIN auth_user AS U ON R.userid_id = U.id";
        $query .= $primero ? " WHERE" : " AND";
        $query .= " U.username = :" . $key;
        $datos[$key] = $value;
        $primero = false;
      }
    }
  }

  // Creamos la query final
  $query = $querystart . $inner . $query . " ORDER BY R.fecha_creacion DESC";

  // Debug para mostrar la query
  //echo $query . "<br>";
  //var_dump($datos);

  // Obtenemos las recetas
  try
  {
    $stmt = $db->prepare($query);
    $stmt->execute($datos);
    $result = $stmt->fetchAll();
  }
  catch(PDOException $e)
  {
    echo $e->getMessage();
    return;
  }

  // Renderizamos el template
  echo $twig->render('list.html', array('recetas' => $result));
  return;
}

// Conseguimos todos los usuarios registrados
$autores = array();

try
{
  $query = "SELECT * FROM auth_user";
  $result = $db->query($query);
}
catch(PDOException $e)
{
  echo $e->getMessage();
}

// Los meto todos en un array para pasarlo luego al template
foreach ($result as $autor)
{
  $autores[] = $autor['username'];
}

// Conseguimos todos los ingredientes de la bbdd
$ingredientes = array();

try
{
  $query = "SELECT * FROM ricorico_ingrediente";
  $result = $db->query($query);
}
catch(PDOException $e)
{
  echo $e->getMessage();
}

// Los meto todos en un array para pasarlo luego al template
foreach ($result as $ingrediente)
{
  $ingredientes[] = $ingrediente['nombre'];
}

// Renderizamos el template
echo $twig->render('search.html', array("ingredientes" => $ingredientes, "autores" => $autores));

?>